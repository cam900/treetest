﻿#include "tree.hpp"
#include <iostream>

int main()
{
    //          A
    //        B   C
    //      D  E    F
    BinTreeNode<char> *root = new BinTreeNode<char>('A');
    root->left = new BinTreeNode<char>('B');
    root->right = new BinTreeNode<char>('C');
    root->left->left = new BinTreeNode<char>('D');
    root->left->right = new BinTreeNode<char>('E');
    root->right->right = new BinTreeNode<char>('F');

    preorder(root); // A, B, D, E, C, F
    std::cout << std::endl;

    inorder(root); // D, B, E, A, C, F
    std::cout << std::endl;

    postorder(root); // D, E, B, F, C, A
    std::cout << std::endl;

    levelorder(root); // A, B, C, D, E, F
    std::cout << std::endl;

    delete_tree(root);
}
