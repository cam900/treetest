#include <iostream>
#include <queue>

// 노드 정의 : 데이터(char), left, right 자식
// 이진 트리 노드
template<class T>
struct BinTreeNode
{
	T data;
	BinTreeNode* left;
	BinTreeNode* right;
	BinTreeNode(T d)
		: data(d)
		, left(nullptr)
		, right(nullptr)
	{}
};

// 전위 순회
template<class T>
void preorder(BinTreeNode<T>* node)
{
	if (node)
	{
		std::cout << node->data << ", ";
		preorder<T>(node->left);
		preorder<T>(node->right);
	}
	//std::cout << std::endl;
}

// 중위 순회
template<class T>
void inorder(BinTreeNode<T>* node)
{
	if (node)
	{
		inorder<T>(node->left);
		std::cout << node->data << ", ";
		inorder<T>(node->right);
	}
	//std::cout << std::endl;
}

// 후위 순회
template<class T>
void postorder(BinTreeNode<T>* node)
{
	if (node)
	{
		postorder<T>(node->left);
		postorder<T>(node->right);
		std::cout << node->data << ", ";
	}
}

// 레벨 순서 순회
template<class T>
void levelorder(BinTreeNode<T>* node)
{
	std::queue<BinTreeNode<T>*> q;
	q.push(node);
	while (!q.empty())
	{
		auto curr = q.front();
		q.pop();
		std::cout << curr->data << ", ";
		if (curr->left)
			q.push(curr->left);
		if (curr->right)
			q.push(curr->right);
	}
}

template<class T>
void delete_tree(BinTreeNode<T>* node)
{
	if (node)
	{
		delete_tree(node->left);
		delete_tree(node->right);
		delete node;
	}
}
